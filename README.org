#+TITLE: Game Development Topics
#+AUTHOR: Franco Eusébio Garcia
#+DATE: [2019-08-16]

* GAMSo: Extensão em Jogos -- Conteúdo

** Leituras

- <https://makegames.tumblr.com/post/1136623767/finishing-a-game>
- <https://www.aaai.org/Papers/Workshops/2004/WS-04-04/WS04-04-001.pdf>
- <http://www.digra.org/wp-content/uploads/digital-library/05164.51146.pdf>

** Definições

Jogos, brinquedos, simulações...

** Documento de Design

- GDD

** Partes

- Tétrade.

** Decisões

- Problema / escolhas com significado;
- Decisões ao longo do tempo.

** Mecânicas

- Steering mechanics.

** Concreto / abstrato

** Jogos simétricos / assimétricos

** Espaços discretos / contínuos

** Estruturas

Ernest Adam: "The Structure of a Video Game"

- Mecânicas: clockwork design;
  + <https://gamemechanicexplorer.com/#>
    * <https://gamemechanicexplorer.com/ideas>
  + <https://boardgamegeek.com/browse/boardgamemechanic>
  + <http://boardgaming.com/mechanics>
  + Idéias:
    * <http://www.squidi.net/three/index.php>
    * <https://old.reddit.com/r/gamedev/comments/1p6oki/is_there_anywhere_a_list_of_game_programming/>
    * <http://inventwithpython.com/blog/2012/02/20/i-need-practice-programming-49-ideas-for-game-clones-to-code/>
    * <https://www.gamefromscratch.com/post/2013/08/01/Just-starting-out-what-games-should-I-make.aspx>
  + Advanced Game Design: patterns.
- Jogos:
  + MDA Design Framework.
- História:
  + 3-act structure e outras estruturas narrativas.
  + Character arcs.
- Arte:
  + Animação por frames;
  + Animação skeletal.
    * <http://dragonbones.com/en/index.html>
  + <https://easings.net/en>
- Tecnologia:
  + Patterns: <http://gameprogrammingpatterns.com/>

** Game Engine Architecture

- Ilustração com partes que compõe um motor.

** Core Mechanics

** Level Design

** Design sistêmico

- Core loops;
- Economias, Machinations.

** Emergent Gameplay

** Flow

** Balanceamento

** Testes

- Pessoas que nunca jogaram.
- Objetivo do teste (menor protótipo possível para realização).

** Prototipação

- Diferentes cortes.

** Temas

*** Arte e Placeholders

**** Vértices (3D para jogos vs 3D para renderizações)

*** História e Narrativa

** Mundo de Jogo e Interfaces de Usuário

*** Convenções para Entrada

- Mapeamentos de controles, esquemas de controle.

*** Ensinando a Jogar / Tutoriais

*** Localização / Internacionalização

** Monetização, Marketing
